ngCycle = angular.module('ngCycle', [])

ngCycle.directive 'cycle', ($interval)->
  return {
    restrict: 'A'
    scope: {
      cycle: '@'
      pager: '@'
      prev:  '@'
      next:  '@'
    }
    template: """
          <div class="cycle-slideshow">
            <div class="cycle-slides" ng-transclude></div>
            <div ng-if="pager" class="cycle-pager">
                <span ng-class="{active: isActive($index)}" ng-repeat="slide in slides" ng-click="go($index)">•</span>
            </div>
            <div ng-if="prev" class="cycle-prev" ng-click="prevSlide()">&lsaquo;</div>
            <div ng-if="next" class="cycle-next" ng-click="nextSlide()">&rsaquo;</i></div>
          </div>
              """
    controller: ($scope, $element)->
      $scope.slides = []

      @fx = $scope.cycle
      @getCurrentIndex = ()-> $scope.currentIndex

      @addSlide = (element)->
        $scope.slides.push(element)

        if $scope.slides.length == 1
          $placeholder = angular.element( element.html() ).addClass("placeholder")
          $element.children().prepend($placeholder)
          console.log($element.children())

        return $scope.slides.length

      return

    replace: true
    transclude: true

    link: (scope, element, attrs)->
      scope.timeout   = parseInt(attrs.timeout)
      scope.loopCount = parseInt(attrs.loop)
      scope.loop      = parseInt(attrs.loop)

      if attrs.startingSlide?
        scope.currentIndex = parseInt(attrs.startingSlide)
      else
        scope.currentIndex = 1

      scope.isActive = ($index)-> scope.currentIndex ==  $index + 1
      scope.go = ($index)-> scope.currentIndex = $index + 1

      scope.prevSlide = ()->
        scope.currentIndex = scope.currentIndex - 1
        if scope.currentIndex < 1
          scope.currentIndex = scope.slides.length

      scope.nextSlide = ()->
        scope.currentIndex = scope.currentIndex + 1
        if scope.currentIndex > scope.slides.length
          if scope.loop
            checkLoop()
          else
            scope.currentIndex = 1

      checkLoop = ()->
        scope.loopCount = scope.loopCount - 1
        if scope.loopCount < 1
          $interval.cancel(loopSlide)
          scope.currentIndex = scope.currentIndex - 1
        else
          scope.currentIndex = 1
        return

      if not attrs.paused?
        loopSlide = $interval( ()->
                      scope.nextSlide()
                  , scope.timeout)

   }


ngCycle.directive 'cycleSlide', ()->
  return {
    restrict: 'A'
    template: """
          <div ng-class="className" ng-transclude ng-show="show()" ></div>
              """
    # replace: true
    transclude: true
    require: '^cycle'
    scope: true
    controller : ($scope)->

    link: (scope, element, attrs, cycleCtrl)->
      index = cycleCtrl.addSlide(element)
      scope.className = "cycle-slide #{attrs.class} #{cycleCtrl.fx}"
      scope.show = ()->
        return index == cycleCtrl.getCurrentIndex()

  }